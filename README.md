# S4BXI log viewer

*[online version](https://s4bxi.julien-emmanuel.com/log-viewer)*

Vue.JS web utility to visualize [S4BXI](https://s4bxi.julien-emmanuel.com) logs in sequence-diagram form. The display can be downloaded in SVG format