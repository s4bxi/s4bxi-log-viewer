module.exports = {
    publicPath: '/log-viewer/',
    configureWebpack: {
        devServer: {
            watchOptions: {
                ignored: [/public/, /node_modules/],
            }
        }
    }
}
