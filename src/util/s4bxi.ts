import { parse, ParseResult } from "papaparse";

export enum OperationType {
    E2E_ACK,
    PTL_ACK,
    PTL_RESPONSE,
    PTL_PUT,
    PTL_GET,
    PTL_ATOMIC,
    PTL_FETCH_ATOMIC,
    PTL_FETCH_ATOMIC_RESPONSE,
    PCI_PIO_PAYLOAD,
    PCI_DMA_PAYLOAD,
    PCI_DMA_REQUEST,
    PCI_PAYLOAD_WRITE,
    PCI_EVENT,
    PCI_COMMAND,
    COMPUTE
}

export interface LogFile {
    name: string;
    path: string;
}

interface RawOperation {
    operation_type: OperationType;
    initiator_nid: number;
    target_nid?: number;
    start_time: string;
    end_time: string;
}
export interface OperationRow {
    operation_type: OperationType;
    initiator_nid: number;
    target_nid?: number;
    start_time: number;
    end_time: number;
}

// In all of the following, +Number(__).toFixed(9) is used to get a number rounded to the nanosecond:
// `Number(__)` gives a number, 
// `.toFixed(9)` gives a string with the desired precision, 
// and the first `+` makes a Number again

export const parseCsv = (csv: string): Promise<OperationRow[]> =>
    new Promise(resolve =>
        parse(csv, {
            header: true,
            complete: (results: ParseResult<RawOperation>) => resolve(results.data.map(o => ({
                ...o,
                start_time: +Number(o.start_time).toFixed(9),
                end_time: +Number(o.end_time).toFixed(9)
            }))),
        })
    );

// Only look at the 10 last values, this is a good enough heuristic and saves a lot of computation
export const maxTime = (source: OperationRow[]): number =>
    +Number(source.slice(-10).reduce((max, op) => (op.end_time > max ? op.end_time : max), 0)).toFixed(9);

// Only look at the 10 first values (same as max)
export const minTime = (source: OperationRow[]): number =>
    +Number(source.slice(0, 10).reduce((min, op) => (op.start_time < min || min === 0 ? op.start_time : min), 0)).toFixed(9);
