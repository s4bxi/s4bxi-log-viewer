import { OperationRow, OperationType } from "./s4bxi";

export interface SvgActor {
    nid: number;
    svgId: number;
}

interface LoggedActors {
    [name: number]: boolean;
}

export interface SvgDimensions {
    globalWidth: number;
    globalHeight: number;
}

const operationText = {
    [OperationType.E2E_ACK]: "ACK E2E",
    [OperationType.PTL_ACK]: "ACK Portals",
    [OperationType.PTL_RESPONSE]: "Get response",
    [OperationType.PTL_PUT]: "Put request",
    [OperationType.PTL_GET]: "Get request",
    [OperationType.PTL_ATOMIC]: "Atomic request",
    [OperationType.PTL_FETCH_ATOMIC]: "Fetch atomic request",
    [OperationType.PTL_FETCH_ATOMIC_RESPONSE]: "Fetch atomic response",
    [OperationType.PCI_PIO_PAYLOAD]: "PIO payload read",
    [OperationType.PCI_DMA_PAYLOAD]: "DMA payload read",
    [OperationType.PCI_DMA_REQUEST]: "PCI DMA request",
    [OperationType.PCI_PAYLOAD_WRITE]: "PCI payload write",
    [OperationType.PCI_EVENT]: "PCI event",
    [OperationType.PCI_COMMAND]: "PCI command",
    [OperationType.COMPUTE]: "Computation"
};

const operationDasharray = {
    [OperationType.E2E_ACK]: "1, 1",
    [OperationType.PTL_ACK]: "4, 1",
    [OperationType.PTL_RESPONSE]: "",
    [OperationType.PTL_PUT]: "",
    [OperationType.PTL_GET]: "",
    [OperationType.PTL_ATOMIC]: "",
    [OperationType.PTL_FETCH_ATOMIC]: "",
    [OperationType.PTL_FETCH_ATOMIC_RESPONSE]: "",
    [OperationType.PCI_PIO_PAYLOAD]: "2, 1",
    [OperationType.PCI_DMA_PAYLOAD]: "2, 1",
    [OperationType.PCI_DMA_REQUEST]: "",
    [OperationType.PCI_PAYLOAD_WRITE]: "2, 1",
    [OperationType.PCI_EVENT]: "",
    [OperationType.PCI_COMMAND]: "",
    [OperationType.COMPUTE]: ""
};

export const operationColor = {
    [OperationType.E2E_ACK]: "red",
    [OperationType.PTL_ACK]: "green",
    [OperationType.PTL_RESPONSE]: "orange",
    [OperationType.PTL_PUT]: "blue",
    [OperationType.PTL_GET]: "teal",
    [OperationType.PTL_ATOMIC]: "mediumpurple",
    [OperationType.PTL_FETCH_ATOMIC]: "darkmagenta",
    [OperationType.PTL_FETCH_ATOMIC_RESPONSE]: "indigo",
    [OperationType.PCI_PIO_PAYLOAD]: "grey",
    [OperationType.PCI_DMA_PAYLOAD]: "grey",
    [OperationType.PCI_DMA_REQUEST]: "grey",
    [OperationType.PCI_PAYLOAD_WRITE]: "grey",
    [OperationType.PCI_EVENT]: "grey",
    [OperationType.PCI_COMMAND]: "grey",
    [OperationType.COMPUTE]: "chartreuse"
};

export const actorWidth = 50;
export const topOffset = 15;

export interface OperationTextType {
    id: OperationType;
    name: string;
}

export const getActors = (rows: OperationRow[]): SvgActor[] => {
    const actors: SvgActor[] = [];
    const loggedActors: LoggedActors = {};
    let currentSvgId = 0;

    rows.map(row => {
        if (!loggedActors.hasOwnProperty(row.initiator_nid)) {
            loggedActors[row.initiator_nid] = true;
            actors.push({ nid: row.initiator_nid, svgId: currentSvgId++ });
        }

        if (row.target_nid && !loggedActors.hasOwnProperty(row.target_nid)) {
            loggedActors[row.target_nid] = true;
            actors.push({ nid: row.target_nid, svgId: currentSvgId++ });
        }
    });

    return actors;
};

export const actorXcenter = (actor: SvgActor): number => {
    return 15 + 10 + 50 * actor.svgId;
};

export const getOperationColor = (op: OperationRow): string => operationColor[op.operation_type];

export const getOperationText = (op: OperationRow): string => operationText[op.operation_type];

export const getOperationDasharray = (op: OperationRow): string => operationDasharray[op.operation_type];

export const allOperationTypes = (): OperationTextType[] =>
    Object.keys(operationText).map(
        (k: string): OperationTextType => ({
            id: parseInt(k),
            name: operationText[<OperationType>parseInt(k)],
        })
    );
